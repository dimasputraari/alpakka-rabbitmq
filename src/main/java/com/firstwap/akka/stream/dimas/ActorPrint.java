package com.firstwap.akka.stream.dimas;

import akka.actor.AbstractActor;
import akka.actor.Props;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.stream.alpakka.amqp.IncomingMessage;

import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class ActorPrint extends AbstractActor {
    private LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);

    static public Props props(String message, CompletionStage<List<IncomingMessage>> isi) {
        return Props.create(ActorPrint.class, () -> new ActorPrint(message, isi));
    }

    static public class WhoToGreet {
        public final String who;

        public WhoToGreet(String who) {
            this.who = who;
        }
    }

    static public class Greet {
        public Greet() {
        }
    }
    //#greeter-messages

    private final String message;
    private final CompletionStage<List<IncomingMessage>> isi;
    private String greeting = "";

    public ActorPrint(String message, CompletionStage<List<IncomingMessage>> isi) {
        this.message = message;
        this.isi = isi;
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(Greet.class, x -> {
                    this.isi.toCompletableFuture()
                            .get( 100000, TimeUnit.MILLISECONDS)
                            .stream()
                            .map(m -> m.bytes().utf8String())
                            .collect(Collectors.toList()).stream().forEach(System.out::println);
                })
                .build();
    }
}
