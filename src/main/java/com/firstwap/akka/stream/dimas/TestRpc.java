package com.firstwap.akka.stream.dimas;

import akka.Done;
import akka.NotUsed;
import akka.actor.ActorSystem;
import akka.japi.Pair;
import akka.stream.ActorMaterializer;
import akka.stream.Materializer;
import akka.stream.alpakka.amqp.*;
import akka.stream.alpakka.amqp.javadsl.AmqpRpcFlow;
import akka.stream.alpakka.amqp.javadsl.AmqpSink;
import akka.stream.alpakka.amqp.javadsl.AmqpSource;
import akka.stream.javadsl.Flow;
import akka.stream.javadsl.Keep;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;
import akka.stream.testkit.TestSubscriber;
import akka.stream.testkit.javadsl.TestSink;
import akka.testkit.javadsl.TestKit;
import akka.util.ByteString;
import org.junit.AfterClass;
import org.junit.BeforeClass;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletionStage;

public class TestRpc {
    private static ActorSystem system;
    private static Materializer materializer;

    @BeforeClass
    public static void setup() {
        system = ActorSystem.create();
        materializer = ActorMaterializer.create(system);
    }

    @AfterClass
    public static void teardown() {
        TestKit.shutdownActorSystem(system);
    }
    //local connection
    //private AmqpConnectionProvider connectionProvider = AmqpLocalConnectionProvider.getInstance();

    //uri connection
    private AmqpConnectionProvider connectionProvider = AmqpUriConnectionProvider
            .create("amqp://gede:gede@10.32.9.187:5672/gede");

    final String exchangeName = "amqp-conn-it-test-pub-sub-";

    final ExchangeDeclaration exchangeDeclaration =
            ExchangeDeclaration.create(exchangeName, "fanout");

    final String queueName = "amqp-conn-it-test-simple-queue-";
    final QueueDeclaration queueDeclaration = QueueDeclaration.create(queueName);

    final String routingKey = "test";

    final List<String> input = Arrays.asList("one", "two", "three", "four", "five");

    Sink<ByteString, CompletionStage<Done>> amqpSink;

    public static void main(String[] args) {
        try {
            TestRpc cobaSink = new TestRpc();
            system = ActorSystem.create();
            materializer = ActorMaterializer.create(system);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void testRpc() {
        final Flow<ByteString, ByteString, CompletionStage<String>> ampqRpcFlow =
                AmqpRpcFlow.createSimple(
                        AmqpSinkSettings.create(connectionProvider)
                                .withRoutingKey(queueName)
                                .withDeclarations(queueDeclaration),
                        1);

        final Integer bufferSize = 10;
        final Source<IncomingMessage, NotUsed> amqpSource =
                AmqpSource.atMostOnceSource(
                        NamedQueueSourceSettings.create(connectionProvider, queueName)
                                .withDeclarations(queueDeclaration),
                        bufferSize);

        final List<String> input = Arrays.asList("one", "two", "three", "four", "five");
        // #run-rpc-flow
        Pair<CompletionStage<String>, TestSubscriber.Probe<ByteString>> result =
                Source.from(input)
                        .map(ByteString::fromString)
                        .viaMat(ampqRpcFlow, Keep.right())
                        .toMat(TestSink.probe(system), Keep.both())
                        .run(materializer);
        // #run-rpc-flow
        //result.first().toCompletableFuture().get(3, TimeUnit.SECONDS);

        Sink<OutgoingMessage, CompletionStage<Done>> amqpSink =
                AmqpSink.createReplyTo(AmqpReplyToSinkSettings.create(connectionProvider));

        amqpSource
                .map(
                        b ->
                                new OutgoingMessage(
                                        b.bytes().concat(ByteString.fromString("a")),
                                        false,
                                        false,
                                        Optional.of(b.properties()),
                                        Optional.empty()))
                .runWith(amqpSink, materializer);

        result
                .second()
                .request(5)
                .expectNextUnordered(
                        ByteString.fromString("onea"),
                        ByteString.fromString("twoa"),
                        ByteString.fromString("threea"),
                        ByteString.fromString("foura"),
                        ByteString.fromString("fivea"))
                .expectComplete();
    }

}
