package com.firstwap.akka.stream.dimas;

import io.micrometer.core.instrument.Clock;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.influx.InfluxConfig;
import io.micrometer.influx.InfluxMeterRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class Metrics {
    @Autowired
    InfluxConfig influxConfig;

    @Bean
    MeterRegistry meterRegistry(){
        return new InfluxMeterRegistry(influxConfig, Clock.SYSTEM);
    }
}
