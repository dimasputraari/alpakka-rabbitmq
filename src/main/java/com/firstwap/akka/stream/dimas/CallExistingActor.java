package com.firstwap.akka.stream.dimas;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;

public class CallExistingActor {

    public void main(String[] args) {
        ActorSystem actorSystem = ActorSystem.create("system");
        ActorRef actorRef = actorSystem.actorOf(Props.create(ActorManusia.class,"manusia"));

        actorRef.tell("aku",ActorRef.noSender());
    }
}
