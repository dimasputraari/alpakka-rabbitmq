package com.firstwap.akka.stream.dimas;

import akka.actor.AbstractActor;

import java.util.ArrayList;
import java.util.List;

public class ActorFlow extends AbstractActor {

    ActorFlow(){}

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(String.class, word -> {
                    CobaSink cobaSink = new CobaSink();
                    char[] charArray = word.toCharArray();
                    List<String> chars = new ArrayList<>();
                    for(char row: charArray) {
                        chars.add(""+row);
                    }
                    cobaSink.sink(chars);
                })
                .build();
    }
}
