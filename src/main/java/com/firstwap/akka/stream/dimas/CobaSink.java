package com.firstwap.akka.stream.dimas;

import akka.Done;
import akka.NotUsed;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.stream.ActorMaterializer;
import akka.stream.Materializer;
import akka.stream.alpakka.amqp.*;
import akka.stream.alpakka.amqp.javadsl.AmqpSink;
import akka.stream.alpakka.amqp.javadsl.AmqpSource;
import akka.stream.javadsl.*;
import akka.util.ByteString;
import akka.util.Timeout;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.*;

@Component
public class CobaSink {
    private static ActorSystem system;
    private static Materializer materializer;
    //local connection
    //private AmqpConnectionProvider connectionProvider = AmqpLocalConnectionProvider.getInstance();

    //uri connection
    private AmqpConnectionProvider connectionProvider = AmqpUriConnectionProvider
            .create("amqp://dimas:dimas@10.32.9.187:5672/dimas");

    //create exchange name
    final String exchangeName = "amqp-conn-it-test-pub-sub-";
    //create exchange
    final ExchangeDeclaration exchangeDeclaration =
            ExchangeDeclaration.create(exchangeName, "topic");

    //create queue name
    final String queueName = "amqp-conn-it-test-simple-queue-";
    //create queue
    final QueueDeclaration queueDeclaration = QueueDeclaration.create(queueName);

    //create queue name
    final String queueName2 = "testing-queue";
    //create queue
    final QueueDeclaration queueDeclaration2 = QueueDeclaration.create(queueName2).withDurable(true);

    //create routing
    final String routingKey = "routing";

    //create routing
    final String routingKey2 = "bedalagi";

    //buffer size
    final Integer bufferSize = 50;

    //sample input file
    final List<String> input = Arrays.asList("one");

    //sink 1
    Sink<ByteString, CompletionStage<Done>> amqpSink;

    //sink 2
    Sink<ByteString, CompletionStage<Done>> amqpSink2;

    //source 1
    Source<IncomingMessage, NotUsed> amqpSource;

    //declare actorFlow
    private static ActorRef actorFlow;

    @Autowired
    private Metrics metrics;

    @PostConstruct
    public void postConstruct(){
        try {
            system = ActorSystem.create();
            materializer = ActorMaterializer.create(system);
            try {
                 consume();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        } catch (InterruptedException e){
            e.printStackTrace();
        }
        Source.range(1, 1_000_000).map(i -> ByteString.fromString(String.valueOf(i))).runWith(amqpSink, materializer);
    }




    CobaSink(){
        system = ActorSystem.create();
        materializer = ActorMaterializer.create(system);
        amqpSink =
                AmqpSink.createSimple(
                        AmqpSinkSettings.create(connectionProvider)
                                .withRoutingKey(routingKey)
                                .withExchange(exchangeName)
                                .withDeclarations(queueDeclaration)

                );
        amqpSink2 =
                AmqpSink.createSimple(
                        AmqpSinkSettings.create(connectionProvider)
                                .withRoutingKey(routingKey2)
                                .withExchange(exchangeName)
                                .withDeclarations(queueDeclaration2));
        amqpSource =
                AmqpSource.atMostOnceSource(
                        NamedQueueSourceSettings.create(connectionProvider, queueName)
                                .withDeclarations(queueDeclaration),
                        bufferSize);
    }

    public void publish() throws ExecutionException, InterruptedException{
        //Publish
        Source.from(input).map(ByteString::fromString).runWith(amqpSink, materializer);
    }

    public void consume() throws ExecutionException, InterruptedException {

        //Consume with actor flow
        Timeout askTimeout = Timeout.apply(5, TimeUnit.SECONDS);
        actorFlow = system.actorOf(Props.create(ActorFlow.class),"test");

      //  final Counter processed = metrics.counter("processed");
        amqpSource.map(i -> i.bytes().utf8String())
//                .map(ByteString::fromString)
                .flatMapMerge(1,bs -> Source.from(Arrays.asList(bs.split("(?!^)"))).map(ByteString::fromString))
                .runWith(amqpSink2, materializer);
        /*
        .runForeach(item -> {
            processed.increment();
            System.out.println("Processed " + item);
        }, materializer);
        */
//                .mapAsync(5,elem -> ask(actorFlow, elem, askTimeout))
//                .runForeach(i -> System.out.println(i), materializer)
//                .map(i -> (List<ByteString>) i)
                //.runWith(amqpSink2, materializer);
;
    }

    public void sink(List<String> isi) {

        Source.from(isi).map(ByteString::fromString).runWith(amqpSink2, materializer);

    }

}
