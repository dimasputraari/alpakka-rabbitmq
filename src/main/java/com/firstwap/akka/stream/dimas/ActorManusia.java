package com.firstwap.akka.stream.dimas;

import akka.actor.AbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;

public class ActorManusia extends AbstractActor {
    private LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);

    @Override
    public Receive createReceive() {
        return receiveBuilder()
            .match(String.class, x -> {
                log.info(x);
                System.out.println(x);
            })
            .build();
    }
}
